# aplikacja-dajpanczy

## What to install

- python (minimum 3.6.6)
- sqlite (minimum 3.22.0)
- django (django-2.1.3 installed by pip)
- django-crispy-forms (install by pip) **Bootstrap**
- setup-tools (install byb pip)
- python-dotenv
- social-auth-app-django (install byb pip)
- python-jose (install byb pip)

## How to start server

```
python3 manage.py runserver 127.0.0.1:8000
```

## How to test application

```
python3 manage.py test
```

## Account in Auth0

**Login**: psi2019@wp.pl

**Password**: Dupa1234

## Admin account

**Acc name**: admin

**Password**: admin123

**Mail**: admin@example.com

## Student account

**Acc name**: student18

**Password**: dupa1234

**Mail**: student18@pwr.pl

#### For auth0 login:

**Login**: student@pwr.edu.pl

**Password**: Dupa1234

## Prowadzący account

**Acc name**: prowadzacy18

**Password**: dupa1234

**Mail**: prowadzacy18@pwr.pl

#### For auth0 login:

**Login**: prowadzacy@pwr.edu.pl

**Password**: Dupa1234

## Diploma section account

#### For auth0 login:

**Login**: sekcjad@pwr.edu.pl

**Password**: sekcjaD19

## Attorney for the field of study  account
#### For auth0 login:

**Login**: pelnomocnik@pwr.edu.pl

**Password**: Dupa1234

## Docker content

- system (ubuntu18.04)
- python3
- python3-pip
- git
- wget
- openssh-client
- sqlite3
- sqlitebrowser
- vim
- django (installed via pip3)
- django-crispy-forms (installed via pip3)
- setup-tools (install byb pip)
- social-auth-app-django (install byb pip)
- python jose (install byb pip)


