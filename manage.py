#!/usr/bin/env python
import os
import sys

from django.core.management import call_command


if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'psiWeb.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
    call_command('migrate', 'dyplomowanie', verbosity=3, interactive=False)


