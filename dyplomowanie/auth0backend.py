from six.moves.urllib import request
from jose import jwt
from social_core.backends.oauth import BaseOAuth2


class Auth0(BaseOAuth2):
    """Auth0 OAuth authentication backend"""
    name = 'auth0'
    SCOPE_SEPARATOR = ' '
    ACCESS_TOKEN_METHOD = 'POST'
    EXTRA_DATA = [
        ('picture', 'picture'), ('groups', 'groups')
    ]

    def authorization_url(self):
        return "https://" + self.setting('DOMAIN') + "/authorize"

    def access_token_url(self):
        return "https://" + self.setting('DOMAIN') + "/oauth/token"

    def get_user_id(self, details, response):
        """Return current user id."""
        return details['user_id']

    def get_user_details(self, response):
        # Obtain JWT and the keys to validate the signature
        idToken = response.get('id_token')
        jwks = request.urlopen("https://" + self.setting('DOMAIN') + "/.well-known/jwks.json")
        issuer = "https://" + self.setting('DOMAIN') + "/"
        audience = self.setting('KEY') #CLIENT_ID
        payload = jwt.decode(idToken, jwks.read(), algorithms=['RS256'], audience=audience, issuer=issuer)
        groups = ''
        given_name = ''
        family_name = ''
        if 'http://127.0.0.1:8000/dyplomowanie/groups' in payload:
            groups = payload['http://127.0.0.1:8000/dyplomowanie/groups']['groups']
        if 'given_name' in payload:
            given_name = payload['given_name']
        elif 'http://127.0.0.1:8000/dyplomowanie/first_name' in payload:
            given_name = payload['http://127.0.0.1:8000/dyplomowanie/first_name']

        if 'family_name' in payload:
            family_name = payload['given_name']
        elif 'http://127.0.0.1:8000/dyplomowanie/last_name' in payload:
            family_name = payload['http://127.0.0.1:8000/dyplomowanie/last_name']

        print(payload)
        print(groups)
        return {'username': payload['nickname'],
                'first_name': given_name,
                'last_name': family_name,
                'picture': payload['picture'],
                'user_id': payload['sub'],
                'groups': groups,
                }
