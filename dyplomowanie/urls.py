from django.urls import include, path
from . import views

urlpatterns = ([
    path('', views.index, name='index'),
    path('reviewerView', views.reviewerView, name='reviewerView'),
    path('reviewerView/setMark/<int:pk>/', views.setMark, name='setMark'),
    path('listOfTopics', views.listOfTopics, name='listOfTopics'),
    path('addTopic', views.addTopic, name='addTopic'),
    path('reviewerView/<int:pk>/', views.deleteTopic, name='deleteTopic'),
    path('reviewerView/edit/<int:pk>/', views.editTopic, name='editTopic'),
    path('listForRecordings', views.listForRecordings, name='listForRecordings'),
    path('saveFile', views.saveFile, name='saveFile'),
    path('topicsAcceptance', views.topicsAcceptance, name='topicsAcceptance'),
    path('topicStatusChanges/<int:pk>/', views.topicStatusChanges, name='topicStatusChanges'),
    path('topicChoice', views.topicChoice, name='topicChoice'),
    path('topicsPublication', views.topicsPublication, name='topicsPublication'),
    path('', include('django.contrib.auth.urls')),
    path('', include('social_django.urls')),
])

# urlpatterns += i18n_patterns(
#     path(_('about/'), views.index, name='about'),
#     path(_('news/'), include(urlpatterns, namespace='news')),
# )