import os
import time
from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import (BaseCommand, CommandError)

USERNAME_SUPERUSER = settings.DATABASES['default']['USERNAME_SUPERUSER']
PASSWORD_SUPERUSER = settings.PASSWORD_SUPERUSER['default']['PASSWORD_SUPERUSER']
EMAIL_SUPERUSER = settings.EMAIL_SUPERUSER['default']['EMAIL_SUPERUSER']
DATABASE_NAME = settings.DATABASE_NAME['default']['NAME'] #eg: 'db.sqlite3'


class Command(BaseCommand):
    help = 'Command to deploy and backup the latest database.'

    def add_arguments(self, parser):
        parser.add_argument(
            '-b', '--backup', action='store_true',
            help='Just backup command confirmation.'
        )

    def success_info(self, info):
        return self.stdout.write(self.style.SUCCESS(info))

    def error_info(self, info):
        return self.stdout.write(self.style.ERROR(info))

    def handle(self, *args, **options):
        backup = options['backup']

        if not backup:
            return self.print_help()

        # Removing media files, if you need to remove all media files
        # os.system('rm -rf media/images/')
        # self.success_info("[+] Removed media files at `media/images/`")

        # Removing database `db.sqlite3`
        if os.path.isfile(DATABASE_NAME):
            # backup the latest database, eg to: `db.2017-02-03.sqlite3`
            backup_database = 'db.%s.sqlite3' % time.strftime('%Y-%m-%d')
            os.rename(DATABASE_NAME, backup_database)
            self.success_info("[+] Backup the database `%s` to %s" % (DATABASE_NAME, backup_database))

            # remove the latest database
            os.remove(DATABASE_NAME)
            self.success_info("[+] Removed %s" % DATABASE_NAME)

        # Removing all files migrations for `dyplomowanie`
        def remove_migrations(path):
            exclude_files = ['__init__.py', '.gitignore']
            path = os.path.join(settings.BASE_DIR, path)

            filelist = [
                f for f in os.listdir(path)
                if f.endswith('.py')
                and f not in exclude_files
            ]
            for f in filelist:
                os.remove(path + f)
            self.success_info('[+] Removed files migrations for {}'.format(path))

        # do remove all files migrations
        remove_migrations('dyplomowanie/migrations/')

        # Removing all `.pyc` files
        os.system('find . -name *.pyc -delete')
        self.success_info('[+] Removed all *.pyc files.')

        # Creating database migrations
        # These commands should re-generate the new database, eg: `db.sqlite3`
        os.system('python manage.py makemigrations')
        os.system('python manage.py migrate')
        self.success_info('[+] Created database migrations.')

        # Creating a superuser
        user = User.objects.create_superuser(
            username=USERNAME_SUPERUSER,
            password=PASSWORD_SUPERUSER,
            email=EMAIL_SUPERUSER
        )
        user.save()
        self.success_info('[+] Created a superuser for `{}`'.format(USERNAME_SUPERUSER))
