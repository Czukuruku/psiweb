from django.db import models
from .utils import ChoiceEnum
from django.contrib.auth.models import User


class LanguageChoice(ChoiceEnum):
    EN = "angielski"
    PL = "polski"


class AcceptanceStatus(ChoiceEnum):
    Reported = "zgłoszony"
    Accepted = "zaakceptowany"
    ToCorrect = "do korekty"
    Approved = "zatwierdzony"


class StudiesDegree(ChoiceEnum):
    Engineer = "inżynier"
    Master = "magister"


class DomainList(ChoiceEnum):
    machineLearning = "machine learning"
    artificialIntelligence = "sztuczna inteligencja"
    cryptography = "kryptografia"
    network = "network"


class SpecializationList(ChoiceEnum):
    DataScience = "danologia"
    SoftwareEngineering = "inżynieria oprogramowania"
    PSI = "projektowanie systemów informatycznych" # TODO check for name and below too
    ZSTII = "zastosowanie specjalistycznych technologii informatycznych"


class GradeType(ChoiceEnum):
    supervisor = 0
    reviewer = 1
    commission = 2
    final = 3


class PublishStatus(ChoiceEnum):
    Published = "opublikowany"
    Unpublished = "nieopublikowany"


class Student(models.Model):
    studentId = models.AutoField(primary_key=True)
    studentName = models.TextField(max_length=25)
    studentSurname = models.TextField(max_length=50)
    studentCardId = models.TextField(max_length=6)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)


class Study(models.Model):
    studyId = models.AutoField(primary_key=True)
    studentId = models.ForeignKey(Student, on_delete=models.CASCADE, null=True)
    major = models.TextField(default="Informatyka", editable=False)
    specialization = models.TextField(choices=SpecializationList.choices(), null=True)
    degree = models.CharField(max_length=25, choices=StudiesDegree.choices(), default='inżynier')


class Lecturer(models.Model):
    lecturerId = models.AutoField(primary_key=True)
    lecturerName = models.TextField(max_length=25)
    lecturerSurname = models.TextField(max_length=50)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)


class Thesis(models.Model):
    thesisId = models.AutoField(primary_key=True)
    thesisTopic = models.TextField(max_length=60)
    status = models.TextField(choices=AcceptanceStatus.choices(), default="zgłoszony")
    textContent = models.TextField(max_length=250, null=True)
    language = models.CharField(max_length=5, choices=LanguageChoice.choices())
    isTaken = models.BooleanField(default=False)
    supervisor = models.ForeignKey(Lecturer, on_delete=models.CASCADE, related_name="promotor", null=True)
    reviewer = models.ForeignKey(Lecturer, on_delete=models.CASCADE, related_name="recenzent", null=True)
    reviewer2 = models.ForeignKey(Lecturer, on_delete=models.CASCADE, related_name="drugiRecenzent", null=True)   # can be null!
    reviewer3 = models.ForeignKey(Lecturer, on_delete=models.CASCADE, related_name="trzeciRecenzent", null=True)  # can be null
    studyId = models.ForeignKey(Study, on_delete=models.CASCADE, null=True)
    specialization = models.TextField(choices=SpecializationList.choices(), null=True)
    publishStatus = models.TextField(choices=PublishStatus.choices(), default="nieopublikowany")
    major = models.TextField(default="Informatyka", editable=False)


class Domain(models.Model):
    domainId = models.AutoField(primary_key=True)
    domainName = models.TextField(choices=DomainList.choices())


class DomainCombination(models.Model):
    domainCombinationId = models.AutoField(primary_key=True)
    domainId = models.ForeignKey(Domain, on_delete=models.CASCADE)
    thesisId = models.ForeignKey(Thesis, on_delete=models.CASCADE)
    lecturerId = models.ForeignKey(Lecturer, on_delete=models.CASCADE)


class Grade(models.Model):
    gradeId = models.AutoField(primary_key=True)
    thesisId = models.ForeignKey(Thesis, on_delete=models.CASCADE)
    GradeType = models.IntegerField(choices=GradeType.choices())
    gradeValue = models.FloatField()
    substantiation = models.TextField(max_length=250)
    personId = models.ForeignKey(Lecturer, on_delete=models.CASCADE)
