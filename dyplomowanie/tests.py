from django.test import TestCase
from django.test import Client
from django.urls import reverse
from django.contrib.auth.models import User, Group
from .models import *


# Create your tests here.
class ViewsTests(TestCase):
    client = Client()

    def setUp(self):
        topic = Thesis.objects.create(thesisTopic='For test', status='zgłoszony', textContent='test', language='polski',
                                      isTaken=False, specialization='DataScience')
        group = Group.objects.create(name='prowadzacy')
        self.user = User.objects.create_user(username='prowadzacy', first_name='Adam', last_name='Kowal')
        group.user_set.add(self.user)
        Lecturer.objects.get_or_create(lecturerName=self.user.first_name,
                                       lecturerSurname=self.user.last_name,
                                       user=self.user)


    def test_index_view(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 302)

    def test_reviewerView_view(self):
        response = self.client.get(reverse('reviewerView'))
        self.assertEqual(response.status_code, 302)

    def test_listOfTopics_view(self):
        response = self.client.get(reverse('listOfTopics'))
        self.assertEqual(response.status_code, 302)

    def test_addTopic_view(self):
        response = self.client.get(reverse('addTopic'))
        self.assertEqual(response.status_code, 302)

    def test_listForRecordings_view(self):
        response = self.client.get(reverse('listForRecordings'))
        self.assertEqual(response.status_code, 302)

    def test_saveFile_view(self):
        response = self.client.get(reverse('saveFile'))
        self.assertEqual(response.status_code, 200)

    def test_topicsAcceptance_view(self):
        response = self.client.get(reverse('topicsAcceptance'))
        self.assertEqual(response.status_code, 200)

    def test_topicChoice_view(self):
        response = self.client.get(reverse('topicChoice'))
        self.assertEqual(response.status_code, 200)

    def test_topicsPublication_view(self):
        response = self.client.get(reverse('topicsPublication'))
        self.assertEqual(response.status_code, 200)

    def test_setMark_view(self):
        response = self.client.get(reverse('setMark', args=[1]))
        self.assertEqual(response.status_code, 302)

    def test_deleteTopic_view(self):
        response = self.client.get(reverse('deleteTopic', args=[1]))
        self.assertEqual(response.status_code, 302)

    def test_editTopic_view(self):
        response = self.client.get(reverse('editTopic', args=[1]))
        self.assertEqual(response.status_code, 302)

    def test_addingTopic(self):
        topic = Thesis.objects.create(thesisTopic='For test', status='zgłoszony', textContent='test', language='polski',
                                      isTaken=False, specialization='DataScience')

        self.assertTrue(isinstance(topic, Thesis))

    def test_something(self):
        topic = Thesis.objects.update(thesisTopic='For test', status='zgłoszony', textContent='test', language='polski',
                                      isTaken=True, specialization='DataScience')
        self.assertEqual(Thesis.objects.get(thesisTopic='For test').isTaken, True)

    def test_add_topic_from_view(self):
        topic_count = Thesis.objects.count()
        self.client.force_login(self.user)
        response = self.client.post('http://127.0.0.1:8000/dyplomowanie/addTopic', {'thesisTopic': 'For test',
                                                                                    'textContent': 'test',
                                                                                    'language': 'polski',
                                                                                    'student': '0',
                                                                                    'specialization': 'danologia'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Thesis.objects.count(), topic_count+1)

    def test_add_edit_topic_from_view(self):
        self.client.force_login(self.user)
        response = self.client.post('http://127.0.0.1:8000/dyplomowanie/listOfTopics/edit/1/', {'thesisTopic': 'Change',
                                                                                                'textContent': 'test',
                                                                                                'language': 'polski',
                                                                                                'student': '0',
                                                                                                'specialization': 'danologia'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Thesis.objects.get(thesisId=1).thesisTopic, 'Change')
