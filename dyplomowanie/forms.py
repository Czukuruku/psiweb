from django import forms
from .models import *


class TheseForm(forms.ModelForm):

    class Meta:
        model = Thesis
        fields = ('thesisTopic', 'textContent', 'language', 'studyId')

        labels = {
            'thesisTopic': 'Temat',
            'textContent': 'Krótki opis',
            'language': 'Język',
            'major': 'Kierunek'
        }
        print(fields)

    # def __init__(self, *args, **kwargs):
    #     super(TheseForm, self).__init__(*args, **kwargs)
    #     study = Study.objects.all()
    #     print(study)
    #     if study:
    #         self.fields['major'] = forms.ModelChoiceField(queryset=SpecializationList)
