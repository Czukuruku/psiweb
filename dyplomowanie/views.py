from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import get_object_or_404
from django.utils import translation
from django.contrib.auth.models import Group
from .models import *
from django.db.models import Q
import csv
from .forms import TheseForm
from django.db.models import Case, Count, When
from django.conf import settings


@login_required
def index(request):
    user = request.user
    auth0user = user.social_auth.get(provider='auth0')
    groups = auth0user.extra_data['groups']
    print("User groups:")
    print(groups)
    for group_name in groups:
        if group_name == 'prowadzacy':
            lect, create = Lecturer.objects.get_or_create(lecturerName=user.first_name,
                                                          lecturerSurname=user.last_name,
                                                          user=user)
        group, create = Group.objects.get_or_create(name=group_name)
        group.user_set.add(user)
        group.save()
    user.groups.all()
    request.session["lang"] = "pl"
    user_groups = get_user_groups(request)
    return render(request, 'dyplomowanie/index.html', {'groups': user_groups})


@login_required
@user_passes_test(lambda u: u.groups.filter(name='student').count() == 0, login_url='/dyplomowanie/denied/') # TODO add new page for access denied
def setMark(request, pk):
    user_groups = get_user_groups(request)
    current_user = request.user
    lecturer = Lecturer.objects.get(user=current_user)
    # TODO: przypisywac do oceny osobe wystawiajaca ocene + GradeType, poki co zahardcoduje
    these = get_object_or_404(Thesis, pk=pk)
    if Grade.objects.filter(thesisId=these, GradeType=1).exists():
        grade = Grade.objects.filter(thesisId=these, GradeType=1, personId=lecturer)[0]
    else:
        grade = ''
    if request.method == "POST":
        gradeValue = request.POST.get('grade')
        substantiation = request.POST.get('substantiation')
        if these.grade_set.filter(GradeType=1).count() > 0:
            # TODO: póki co biore pierwsza z brzegu, bedziemy trzeba wybrać oocenę o odpowiednim typie
            grade.gradeValue = gradeValue
            grade.substantiation = substantiation
            grade.save()
            these.save()
        else:
            Grade.objects.create(gradeValue=gradeValue, substantiation=substantiation, thesisId=these,
                                 GradeType=1, personId=lecturer)
        thesis = Thesis.objects.filter(Q(supervisor=lecturer) | Q(reviewer=lecturer) |
                                       Q(reviewer2=lecturer) | Q(reviewer3=lecturer))
        context = []
        for these in thesis:
            if Grade.objects.filter(thesisId=these, GradeType=1):
                context.append((these, Grade.objects.filter(thesisId=these, GradeType=1)[0].gradeValue))
            else:
                context.append((these, "-"))
        return render(request, 'dyplomowanie/reviewerView.html', {'data': context, 'groups': user_groups})

    return render(request, 'dyplomowanie/setMark.html', {'these': these, 'grade': grade, 'groups': user_groups})


@login_required
@user_passes_test(lambda u: u.groups.filter(name='student').count() == 0, login_url='/dyplomowanie/denied/')
def reviewerView(request):
    current_user = request.user
    user_groups = get_user_groups(request)
    context = contextForReview(current_user)
    return render(request, 'dyplomowanie/reviewerView.html', {'data': context, 'groups': user_groups})


def contextForReview(current_user):
    if current_user.groups.filter(name='admin').count() == 0:
        lecturer = Lecturer.objects.get(user=current_user)
        thesis = Thesis.objects.filter(Q(supervisor=lecturer) | Q(reviewer=lecturer) |
                                   Q(reviewer2=lecturer) | Q(reviewer3=lecturer))
    else:
        thesis = Thesis.objects.all()
    context = []
    for these in thesis:
        if Grade.objects.filter(thesisId=these, GradeType=1):
            context.append((these, Grade.objects.filter(thesisId=these, GradeType=1)[0].gradeValue))
        else:
            context.append((these, "-"))
    return context


@login_required
@user_passes_test(lambda u: u.groups.filter(name='student').count() == 0, login_url='/dyplomowanie/denied/') # TODO add new page for access denied
def listOfTopics(request):
    current_user = request.user
    if Lecturer.objects.filter(user=current_user).exists():
        lecturer = Lecturer.objects.get(user=current_user)
    else:
        lecturer = None
    user_groups = get_user_groups(request)
    thesis = Thesis.objects.all().annotate(relevancy=Count(Case(When(supervisor=lecturer, then=1))))\
        .order_by('-relevancy').order_by('thesisTopic')
    return render(request, 'dyplomowanie/listOfTopics.html', {'thesis': thesis, 'groups': user_groups})


# TODO: only administrator should have access to this
@login_required
@user_passes_test(lambda u: u.groups.filter(name='student').count() == 0, login_url='/dyplomowanie/denied/')
def listForRecordings(request):
    user_groups = get_user_groups(request)
    thesis = Thesis.objects.filter(isTaken=True, supervisor__isnull=False, studyId__isnull=False)
    return render(request, 'dyplomowanie/createListForRecordings.html', {'thesis': thesis, 'groups': user_groups})


def saveFile(request):
    thesis = Thesis.objects.filter(isTaken=True, supervisor__isnull=False, studyId__isnull=False)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="listOfSupervisorsAndStudents.csv"'
    writer = csv.writer(response)
    writer.writerow(["Temat pracy", "Promoto", "Imie studenta", "Nazwisko studenta", "Numer indeksu studenta",
                     "Stopien studiow"])
    for these in thesis:
        writer.writerow([these.thesisTopic, these.supervisor.lecturerName + these.supervisor.lecturerSurname,
                        these.studyId.studentId.studentName, these.studyId.studentId.studentSurname,
                        these.studyId.studentId.studentCardId, these.studyId.degree])
    return response


def topicsAcceptance(request):
    user_groups = get_user_groups(request)
    if request.method == "POST":
        selectedItems = request.POST.getlist('checks[]')
        thesisToAccept = [Thesis.objects.get(pk=pk) for pk in selectedItems]
        if request.POST.get('accept'):
            for topic in thesisToAccept:
                topic.status = 'zaakceptowany'
                topic.save()
    thesis = Thesis.objects.all()
    return render(request, 'dyplomowanie/topicsAcceptance.html', {'thesis': thesis, 'groups': user_groups})


def topicStatusChanges(request, pk):
    user_groups = get_user_groups(request)
    these = get_object_or_404(Thesis, pk=pk)
    thesis = Thesis.objects.all()
    statuses = [e.value for e in AcceptanceStatus]
    if request.method == "POST":
        these.status = request.POST.get('status')
        these.save()
        return render(request, 'dyplomowanie/topicsAcceptance.html', {'thesis': thesis, 'groups': user_groups})
    return render(request, 'dyplomowanie/changeTopicStatus.html', {'thesis': thesis,
                                                                   'these': these, 'statuses': statuses,
                                                                   'groups': user_groups})

@login_required
@user_passes_test(lambda u: u.groups.filter(name='student').count() == 0, login_url='/dyplomowanie/denied/')
def deleteTopic(request, pk):
    user_groups = get_user_groups(request)
    these = get_object_or_404(Thesis, pk=pk)
    topic_name = these.thesisTopic
    these.delete()
    current_user = request.user
    context = contextForReview(current_user)
    return render(request, 'dyplomowanie/reviewerView.html', {'data': context, 'groups': user_groups})


@login_required
@user_passes_test(lambda u: u.groups.filter(name='student').count() == 0, login_url='/dyplomowanie/denied/')
def editTopic(request, pk):
    user_groups = get_user_groups(request)
    these = get_object_or_404(Thesis, pk=pk)
    study = ''
    students = Student.objects.all()
    if Study.objects.filter(thesis=these).exists():
        study = Study.objects.filter(thesis=these)[0]
        students = students.exclude(studentId=study.studentId.studentId)
    languages = [e.value for e in LanguageChoice]
    languages.remove(these.language)
    specializationList = [e.value for e in SpecializationList]
    if request.method == "POST":
        these.thesisTopic = request.POST.get('thesisTopic')
        these.textContent = request.POST.get('textContent')
        these.language = request.POST.get('language')
        studentId = request.POST.get('student')
        if studentId and studentId != "0":
            these.isTaken = True
            student = Student.objects.get(pk=studentId)
            these.studyId = Study.objects.get(studentId=student)
        these.save()
        current_user = request.user
        context = contextForReview(current_user)
        return render(request, 'dyplomowanie/reviewerView.html', {'data': context, 'groups': user_groups})
    print(specializationList)
    print(these.specialization)
    if these.specialization:
        specializationList.remove(these.specialization)
    return render(request, 'dyplomowanie/editTopic.html', {'specializations': specializationList,
                                                           'study': study, 'these': these,
                                                           'students': students, 'languages': languages,
                                                           'groups': user_groups})


@login_required
@user_passes_test(lambda u: u.groups.filter(name='student').count() == 0, login_url='/dyplomowanie/denied/') # TODO add new page for access denied
def addTopic(request):
    user_groups = get_user_groups(request)
    current_user = request.user
    if current_user.groups.filter(name='admin').count() == 0:
        lecturer = Lecturer.objects.get(user=current_user)
    students = Student.objects.all()
    specializationList = [e.value for e in SpecializationList]
    languages = [e.value for e in LanguageChoice]
    context = {
        'students': students,
        'specialization': specializationList,
        'languages': languages,
        'groups': user_groups,
    }
    if request.method == 'POST':
        if request.POST.get('thesisTopic') and request.POST.get('textContent') and \
                current_user.groups.filter(name='admin').count() == 0:
            postThesis = Thesis()
            postThesis.thesisTopic = request.POST.get('thesisTopic')
            postThesis.textContent = request.POST.get('textContent')
            postThesis.language = request.POST.get('language')

            postThesis.supervisor = lecturer
            postThesis.specialization = request.POST.get('specialization')
            studentId = request.POST.get('student')
            print(studentId)
            if studentId != "0":
                student = Student.objects.get(pk=studentId)
                print(studentId)
                postThesis.studyId = Study.objects.get(studentId=student)
                postThesis.isTaken = True
            postThesis.save()
            thesis = Thesis.objects.all().annotate(relevancy=Count(Case(When(supervisor=lecturer, then=1)))).order_by('-relevancy')
            return render(request, 'dyplomowanie/listOfTopics.html', {'thesis': thesis, 'groups': user_groups})
        else:
            return render(request, 'dyplomowanie/addTopic.html', context=context)    # nie wyszstkie pola zostaly wypelnione
    else:
        return render(request, 'dyplomowanie/addTopic.html', context=context)


# TODO: do tego dostep powinni miec studenci.
def topicChoice(request):
    # make that thesis which are not taken are showing first
    user_groups = get_user_groups(request)
    thesis = Thesis.objects.filter(publishStatus='opublikowany')\
        .annotate(relevancy=Count(Case(When(isTaken=False, then=1)))).order_by('-relevancy')
    return render(request, 'dyplomowanie/topicChoice.html', {'thesis': thesis, 'groups': user_groups})


def topicsPublication(request):
    def changeStatus(thesisToChange, status):
        for t in thesisToChange:
            t.publishStatus = status
            t.save()
    user_groups = get_user_groups(request)
    thesis = Thesis.objects.filter(status__in=['zatwierdzony'])
    if request.method == 'POST':
        selectedItems = request.POST.getlist('checks[]')
        thesisToChange = [Thesis.objects.get(pk=pk) for pk in selectedItems]
        if request.POST.get('cancelPublications'):
            changeStatus(thesisToChange, 'nieopublikowany')
        elif request.POST.get('publish'):
            changeStatus(thesisToChange, 'opublikowany')
        elif request.POST.get('all'):
            changeStatus(thesis, 'opublikowany')
    return render(request, 'dyplomowanie/topicsPublication.html', {'thesis': thesis, 'groups': user_groups})


def set_language(request):
    print("tets")


def get_user_groups(request):
    return request.user.groups.values_list('name', flat=True)



