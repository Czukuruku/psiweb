��    R      �  m   <     �               1     F  	   X  
   b     m     �  $   �     �     �     �     �     �  1   �     %     :     L     \     j     q     ~     �     �  
   �     �  
   �     �     �  S   �     1	     G	  H   ]	     �	     �	  	   �	  
   �	     �	     �	     �	     
     
     
     
     &
     5
     :
     @
     X
     _
     v
     
     �
     �
     �
     �
     �
     �
     �
       	   
          *     >     G     Z     i     y     �     �     �     �     �     �     �     �  _   �     M     Z  :   p     �     �     �     �     �               $     0     D  &   P     w     }     �     �     �  4   �     �     �     �            
        *     C     P     W     e     r  	   �     �  C   �     �     �  ;        D     V     \     b     i     w     �     �     �     �  	   �     �     �     �     �     �     �          !     <     O     o     �     �  	   �     �     �     �     �     �  	   �     �                    #  
   5     @     N  	   W     a     u     �  P   �     �     �  9        A          N       /                2                3   '   R   9         C   5   ?                H   1       8         @      !      %   Q       A   &         B   F      ,   >       O          #   P              +      (   J               K       6       D   $       -       E   	                 "   0   :   <       *      M       4   
      )           I          .                 7   L   =   G   ;      --Choice language-- --Choice specjalization-- --Choice student-- Acceptance of topics Acceptance topics Add topic Add topics Administrator menu Assessment of the thesis  Attorney for the field of study menu Author Busy Cancel Change my password Change status Change the status of marked topics to unpublished Change topic status: Check your inbox. Choice of topic Choise thesis Delete Delete topic Diploma section menu Diplome Edit Edit topic Edit topics Email Sent English Enter new password Enter your email address below, and we'll email instructions for setting a new one. Forgot Your Password? Forgot your password? Generating a list with promoters and students to the subscription system Grade: Language Language: Language:  List of diploma thesis List of thesis for review List of topics Log out Login Major Major: Manage account Mark Name: Password reset complete Polish Preview generated list Promoter Publish tagged topics Publish topics Publish unpublished topics  Reset password Review Review of diploma thesis Review: Reviewer menu Save Save file Send me instructions! Set a new password! Set mark Short description: Specjalization Specjalization: Status Status publication Student id: Student menu Student: Surname: The diploma process Thesis topic Thesis topic: We've emailed you instructions for setting your password. You should receive the email shortly! Welcome user You are not logged in Your new password has been set. You can log in now on the  log in page --Wybierz jezyk-- --Wybiez specjalnosc-- Wybierz studenta Akceptacja tematow Akceptacja tematow Dodaj temat  Dodaj temat Menu administartora Ocena pracy Menu pelnomocnika ds. kierunku studiow Autor Zajety Anuluj Zmien haslo Zmien status Zmien status na nieopublikowane zazanczonych tematow Zmien status tematu: Sprawdz skrzynke Wybor tematu Wybierz temat Usun Usun temat Menu sekcji dyplomowania Dyplomowanie Edycja Edycja tematu Edytuj temat Wyslij e-mail Angielski Podaj nowe haslo Podaj swoj adres e-mail, wyslemy instrukcje jak ustawic nowe haslo. Zapomniales hasla? Nie pamietassz hasla? Generowanie listy promotorow i studentow do systemu zapisow Stopien studiow:  Jezyk Jezyk Jezyk: Lista tematow Lista prac do recenzji Lista tematow Wyloguj Zaloguj  Kierunek Kierunek: Zarzadzanie kontem Ocena Imie: Haslo jest zresetowane Polski Podglad wygenrowanej listy Promotor Publikuj zaznaczone tematy Publikacja tematow Publikuj nieopublikowane tematy Resetowanie hasla Recenzja Recenzje parc Recenzja: Menu recenzenta Zapisz Zapisz plik Wyslij mi instrukcje! Zmien nowe haslo Ocenianie Krotki opis: Specjalnosc Specjalnosc: Status Status publikacji Nr indeksu Menu studenta Student: Nazwisko: Proces dyplomowania Temat pracy Temat pracy: Na Twoj adres e-mail wyslalismy instrukcje. Za chwile powinienes dostac widomosc Witaj uzytkowniku Nie jestes zalogowany Twoje haslo jest zresetowane. Mozesz sie teraz zalogowac  na tej stronie 